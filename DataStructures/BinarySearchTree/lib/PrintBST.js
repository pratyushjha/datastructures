let printTree  =  function(){
  let queue =  [];
  if(! this._root){
    return ' No Root in Tree';
  }
  let formattingHSize =  400;
  let displacementIdx = 5;
  let positionQueue = [];

  queue.push( this._root );
  positionQueue.push(0);
  let finalPrintArr =  [];
  while( true ){
    queueLen = queue.length;
    let str  =  ' '.repeat(formattingHSize);
    let intermediateStr =  ' '.repeat( formattingHSize);
    let innerQueue  = [];



    for( let key = 0 ; key < queueLen;  key++ ){

        let item  = queue[key];
        let position  = positionQueue.shift();

        let formattingIdx =  formattingHSize/2 + position*displacementIdx*2;
        str  =  str.slice(0, formattingIdx) + (item ?item.val:'') + str.slice(formattingIdx);

        if( item ){
          innerQueue.push( item.leftChild);
          positionQueue.push( position - 1);
          if( item.leftChild){
            intermediateStr  =  intermediateStr.slice(0, formattingIdx-displacementIdx) +
                                  ('/') + intermediateStr.slice(formattingIdx-displacementIdx);
          }

        }

        if( item ){
          innerQueue.push( item.rightChild );
          positionQueue.push( position  + 1);
          if( item.rightChild){
            intermediateStr  =  intermediateStr.slice(0, formattingIdx+displacementIdx) +
                                  ('\\') + intermediateStr.slice(formattingIdx+displacementIdx);
          }

        }
    }
    finalPrintArr.push( str );
    finalPrintArr.push( intermediateStr );
    queue =  innerQueue.slice();
    if( queue.filter(val=>val).length == 0 ){
       break;
    }
  }
  for( str of finalPrintArr){
    console.log( str );
  }

}

module.exports = printTree;
