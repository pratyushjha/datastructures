/**
  Binary Search Tree is a special kind of Binary Tree where every value in the
  left sub tree is less than that of node and every value in the right subtree
  is greater than the node. Thus making insertion, deletion , search easy in
  O(log(n)) case.
*/

function getNode(val, leftChild =  null, rightChild = null){
    return {
       val,
       leftChild,
       rightChild
    }
}

function BinarySearchTree(){
  this._root =  null;
};

module.exports  = BinarySearchTree;

BinarySearchTree.prototype =  {
  constructor : BinarySearchTree,
  insert : function(value){
      if(!value){
        throw new Error('Value for insertion not provided');
      }
      let newNode = getNode(value);

      let currNode =  this._root;
      if( !currNode ){
        this._root =  newNode;
      }else{
          while( currNode ){
             if( currNode.val > value ){
               if( currNode.leftChild ){
                 currNode  =  currNode.leftChild;
               }else{
                 currNode.leftChild =  newNode;
                 break;
               }
             }else if( currNode.val < value ){
               if( currNode.rightChild){
                 currNode  =  currNode.rightChild;
               }else{
                 currNode.rightChild  =  newNode;
                 break;
               }
             }else{
               console.log('value exists', newNode);
               break;
             }
          }
      }
  },
  contains : function( value ){
    let currNode =  this._root;
    let doesContain =  false;
    while( currNode ){
        if( currNode.val > value){
          currNode =  currNode.leftChild;
        }else if( currNode.val < value){
          currNode = currNode.rightChild;
        }else{
          doesContain = true;
          break;
        }
    }
    return doesContain;
  },
  remove : function( value ){
    /**
      There are few conditions that needs to be handled before
      First we need to search if the value exists or not.
      Cases to cover :
        1. The node is root Node.
            a. If the node has no child, make the root node null and done.
            b. If the node has one child, make that as root node.
            c. If the node has two child,
                  i. If we make left child as root node, then the right child becomes its right child
                  ii. If we make right child as root node , then the left child becomes its left child
        1. The node is the leaf node.
            We remove the node from the parent and get done.
        2. The node has children.
    */
    var contains =  false,
        parentNode = null,
        currNode   =  this._root,
        childrenCount = 0,
        currNodeRef = null,
        parentNodeRef =  null;

    while(currNode){
        if( currNode.val > value){
          parentNode =  currNode;
          currNode = currNode.leftChild;
        }else if( currNode.val < value){
          parentNode =  currNode;
          currNode = currNode.rightChild;
        }else{
          contains = true;
          break;
        }
    }

    if( contains && currNode ){
        childrenCount =  ( currNode.leftChild ? 1 : 0) +
                         ( currNode.rightChild ? 1 : 0);

        if( currNode ==  this._root){

            switch( childrenCount ){
              case 0 :
                this._root =  null;
                break;

              case 1 :
                this._root =  this._root.leftChild == null ?
                                this._root.leftChild : this._root.rightChild;
                break;

              case 2 :

                // This is slighly tricky
                // What we do is take the right most element from the left
                // subtree or vice versa, make it the root. Any left child of
                // this node needs to be fit in as the right child of the
                // the node's parent. make the previous root's left child its
                // left child and right, its right child.
                //
                currNodeRef =  this._root.leftChild;

                while( currNodeRef.rightChild != null){

                  parentNodeRef = currNodeRef;
                  currNodeRef  = currNodeRef.rightChild;
                }

                if( parentNodeRef != null ){

                      parentNodeRef.rightChild =  currNodeRef.leftChild;
                      currNodeRef.leftChild =  this._root.leftChild;
                      currNodeRef.rightChild =  this._root.rightChild;

                }else{

                    currNodeRef.rightChild  = this._root.rightChild;
                }

                this._root  = currNodeRef;

                break;
            }
        }else{

          switch( childrenCount ){
            case 0 :
              if( parentNode.val > currNode.val ){
                  parentNode.leftChild =  null;
              }else{
                  parentNode.rightChild =  null;
              }
              break;

            case 1 :
                let onlyChild =  this._root.leftChild == null ?
                              this._root.leftChild : this._root.rightChild;

                if( parentNode.val > currNode.val ){
                    parentNode.leftChild =  onlyChild;
                }else{
                    parentNode.rightChild =  onlyChild;
                }
            break;

            case 2 :

              // This is slighly tricky
              // What we do is take the right most element from the left
              // subtree or vice versa, make it the root. Any left child of
              // this node needs to be fit in as the right child of the
              // the node's parent. make the previous root's left child its
              // left child and right, its right child.
              //
              currNodeRef =  currNode.leftChild;
              parentNodeRef = currNode;

              while( currNodeRef.rightChild != null){

                parentNodeRef = currNodeRef;
                currNodeRef  = currNodeRef.rightChild;
              }

              parentNodeRef.rightChild =  currNodeRef.leftChild;
              currNodeRef.leftChild =  currNode.leftChild;
              currNodeRef.rightChild =  currNode.rightChild;

              if( parentNode.val > currNode.val){

                  parentNode.leftChild =  currNodeRef;
              }else{

                  parentNode.rightChild =  currNodeRef;
              }

            break;
          }

        }
    }
  }
}
