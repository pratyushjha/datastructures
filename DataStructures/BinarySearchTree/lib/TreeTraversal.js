var BST = require('./BST.js');

module.exports = {
  inOrderTraversal,
  inOrderTraversalIterative,
  preOrderTraversal,
  preOrderTraversalIterative,
  postOrderTraversal,
  postOrderTraversalIterative,
  levelOrderTraversal,
  spiralTraversal,
  reverseLevelOrderTraversal,
};

/**
 *  http://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/
 *
 *
*/

function inOrderTraversal(){
  if( !this instanceof BST){
    throw new TypeError('Inorder Traversal is applicable on instance of BST class')
  }
  let arr  = [];
  let _inorderTraverse =  (node)=>{
      if(!node) return ;

      _inorderTraverse(node.leftChild);
      arr.push( node.val );
      _inorderTraverse( node.rightChild );
  }
  _inorderTraverse(this._root);
  return arr;
}


/**
 * http://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion/
 *
 */
function inOrderTraversalIterative(){
  if( !this instanceof BST){
    throw new TypeError('Inorder Traversal is applicable on instance of BST class')
  };
  let arr  = [],
      finalArr =  [];
  let current  = this._root;
  while( true ){
    if( current ){
        arr.push( current);
        current = current.leftChild;
    }else if( current == null && arr.length > 0){
        current = arr.pop();

        finalArr.push(current.val);
        current = current.rightChild;
    }else{
        break;
    }
  }
  return finalArr;
}


/**
 *  http://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/
 *
 *
*/

function preOrderTraversal(){
  if( !this instanceof BST){
    throw new TypeError('PreOrder Traversal is applicable on instance of BST class')
  }

  let arr  = [];
  let _preorderTraverse =  (node)=>{
      if(!node) return ;

      arr.push( node.val );
      _preorderTraverse(node.leftChild);
      _preorderTraverse( node.rightChild );
  }
  _preorderTraverse(this._root);
  return arr;

}



function preOrderTraversalIterative(){
  if( !this instanceof BST){
    throw new TypeError('PreOrder Traversal is applicable on instance of BST class')
  }

  let arr  = [],
      finalArr =  [];
      arr.push( this._root);

  while( arr.length > 0 ){
        let current  =  arr.pop();
        if( current ){
          finalArr.push(current.val);
          arr.push( current.rightChild);
          arr.push(current.leftChild);
        }
  }

  return finalArr;
}


/**
 *  http://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/
 *
 *
*/

function postOrderTraversal(){
  if( !this instanceof BST){
    throw new TypeError('PostOrder Traversal is applicable on instance of BST class')
  }

  let arr  = [];
  let _postorderTraverse =  (node)=>{
      if(!node) return ;

      _postorderTraverse(node.leftChild);
      _postorderTraverse( node.rightChild );
      arr.push( node.val );
  }
  _postorderTraverse(this._root);
  return arr;
}


/**
 * http://www.geeksforgeeks.org/iterative-postorder-traversal-using-stack/
 *
 */

function postOrderTraversalIterative(){
  if( !this instanceof BST){
    throw new TypeError('PostOrder Traversal is applicable on instance of BST class')
  };

  let arr  = [],
      finalArr =  [];
  let current  = this._root;
  while( true ){
    if( current ){
        arr.push( current);
        finalArr.push(current.val);
        current = current.leftChild;
    }else if( current == null && arr.length > 0){
        current = arr.pop();
        current = current.rightChild;
    }else{
        break;
    }
  }
  return finalArr;
}


/**
 * It is the Breadth First Search for trees.
 * http://www.geeksforgeeks.org/level-order-tree-traversal/
 * Algorithm :
 *  1. Create a Queue.
 *  2. Insert the root.
 *  3. Loop till the queue is not empty.
 *  4. Pop the element. Print it. Push its left and right children
 */

function levelOrderTraversal(){
  if( !this instanceof BST){
    throw new TypeError('Level Traversal is applicable on instance of BST class')
  }
  let arr =  [],
      finalArr = [];

      arr.push( this._root);

      while( arr.length > 0){
          let node = arr.pop();

          if( node.leftChild ){
              finalArr.push(node.leftChild);
          }
          if( node.rightChild ){
              finalArr.push( node.rightChild);
          }
      }
      return finalArr;
};

/**
 * http://www.geeksforgeeks.org/level-order-tree-traversal/
 *
 */

function levelOrderTraversalUsingRecursion(){
    if( !this instanceof BST){
      throw new TypeError('Level Traversal is applicable on instance of BST class')
    }
    //TODO :

};


/**
 *  http://www.geeksforgeeks.org/level-order-traversal-in-spiral-form/
 */

function spiralTraversal(){
  if( !this instanceof BST){
    throw new TypeError('Spiral Traversal is applicable on instance of BST class')
  }
  let ltr  = false;
  let arr  = [],
      finalArr = [],
      innerArr = [];
  arr.push( this._root);

  while( true ){
      innerArr = [];
      if( ltr ){
        for( let i =0, len = arr.length; i< len; i++){
            let node  = arr[i];
            finalArr.push(node.val);
            if( node.leftChild ){
              innerArr.push(node.leftChild);
            }
            if( node.rightChild){
              innerArr.push(node.rightChild);
            }
        }
      }else{
        for( let len = arr.length,i = len-1; i > -1; i--){
            let node  = arr[i];
            finalArr.push(node.val);
            if( node.leftChild ){
              innerArr.push(node.leftChild);
            }
            if( node.rightChild){
              innerArr.push(node.rightChild);
            }
        }
      }
      arr = innerArr.slice();
      if( arr.length == 0 ){
        break;
      }
      ltr = !ltr;
  }
  return finalArr;
}


/**
 *  http://www.geeksforgeeks.org/reverse-level-order-traversal/
 */

function reverseLevelOrderTraversal(){
  if( !this instanceof BST){
    throw new TypeError('Level Traversal is applicable on instance of BST class')
  }

}
