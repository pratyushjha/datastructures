//http://www.geeksforgeeks.org/construct-bst-from-given-preorder-traversa/

/**
 *
 * @param arr
 * In case of pre-order TreeTraversal,
 * First element is always root and then subsequent array elements till the point it reaches
 * This solution is not optimized as O(n^2) is complexity achieved.
 */

function constructBinarySearchTree(arr) {

    function traverse( arr, startIdx, endIdx ){
        if( startIdx > endIdx){
          return ;
        }
        if( startIdx == endIdx){
          return { val : arr[startIdx], leftChild:null, rightChild:null };
        }
        let rootVal =  arr[startIdx],
            breakIdx =  -1,
            rootNode = { val : rootVal, leftChild:null, rightChild:null };

        for(let i = startIdx; i <= endIdx; i++){
            if( rootVal < arr[i]){
                breakIdx = i-1;
                break;
            }
        }
        if( breakIdx > -1){
            rootNode.leftChild =  traverse( arr, startIdx+1, breakIdx);
            rootNode.rightChild =  traverse( arr, breakIdx+1, endIdx);
        }
        return rootNode;
    }
    return traverse( arr, 0, arr.length-1);
}

/**
 * Give the nodes a range and if it falls in the range then the node
 * is part of the subtree.
 * This is an O(n) solution
 *
*/
let index = {
    idx:0
};
function constructBST( arr ){
    return constructBSTUtil( arr,  index, arr[0], Number.MAX_VALUE, Number.MIN_VALUE);
}

function constructBSTUtil( arr, index, key, max, min){
      if( index.idx > arr.length){
        return null;
      }
      let rootNode = null;
      if( min < key && key < max){
        rootNode  = { val : key, leftChild:null, rightChild: null};
        index.idx += 1;

        if( index.idx < arr.length){
          rootNode.leftChild =  constructBSTUtil( arr, index, arr[index.idx], key, min );
          rootNode.rightChild =  constructBSTUtil( arr, index, arr[index.idx], max, key );
        }
      }
      return rootNode;
}

module.exports  = constructBST;
