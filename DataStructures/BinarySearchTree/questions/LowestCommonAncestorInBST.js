//http://www.geeksforgeeks.org/lowest-common-ancestor-in-a-binary-search-tree/



module.exports = function( num1, num2){
			findLowestCA( this._root, num1, num2);
			return lowestCA.val;
}
let lowestCA = null;

function findLowestCA( node, num1, num2){
		if(!node) return ;

		let val =  node.val;

		if( val < num1 && val < num2){
				lowestCA =  node;
				findLowestCA( node.rightChild, num1, num2);

		}else if( val > num1 && val > num2){
				lowestCA = node;
				findLowestCA( node.leftChild, num1, num2);

		}else if( val < num1 && val > num2){
				lowestCA = node;
				return ;

		}else if( val > num1 && val < num2){
				lowestCA = node;
				return ;

		}
}
