//http://www.geeksforgeeks.org/add-greater-values-every-node-given-bst/

let addAllGreaterValue = function(){
		let sum  =  0;
		function recursive(node){
				if(!node) return 0;

				recursive( node.rightChild);
				node.val += sum;
				sum  = node.val;
				recursive( node.leftChild );

				return node.val;
		}
		recursive( this._root, 0 );
}

module.exports  = addAllGreaterValue;
