//http://www.geeksforgeeks.org/inorder-predecessor-successor-given-key-bst/

let preAndSuccObj = {
		pre : null,
		suc : null
};

/**
 * We do this recusrively. Start from root
 * if the key value is greater than root's value, then we set the predecessor
 * as root and recusrively call root's right child. Similarly, for the
 * left subtree
 * If we find the node , then we look for left subtree's rightmost element or
 * left subtree for predecessor and subsequentyl same for successor on right
 * subtree
 */
function recursiveFnToFindPreAndSuccessor(key, node){
			if( !node ) return null;

			if( key < node.val){
					preAndSuccObj.suc = node.val;
					recursiveFnToFindPreAndSuccessor( key, node.leftChild);
			}else if ( key > node.val ){
					preAndSuccObj.pre = node.val;
					recursiveFnToFindPreAndSuccessor( key, node.rightChild);
			}else{
					if( node.leftChild ){
							let temp  = node.leftChild;
							while( temp.rightChild!=null){
									temp =  temp.rightChild;
							}
							preAndSuccObj.pre =  temp.val;
					}
					if( node.rightChild ){
							let temp  = node.rightChild;
							while( temp.leftChild!=null){
									temp =  temp.leftChild;
							}
							preAndSuccObj.suc =  temp.val;
					}
				//  break;
			}
};

module.exports  = function findPreAndSuccessor( key ){
		if( this._root == null){
			return [];
		}
		recursiveFnToFindPreAndSuccessor( key, this._root);

		console.log( preAndSuccObj.pre , preAndSuccObj.suc);
};
