var BinarySearchTree = require('./lib/BST.js');
var printTree  = require('./lib/PrintBST.js');
var TreeTraversal =  require('./lib/TreeTraversal.js');
var AddGreaterValuesToBST =  require('./questions/AddGreaterValuesToBST.js');
var constructBinarySearchTree = require('./questions/ConstructBSTFromPostOrder.js');
var findPreAndSuccessor = require('./questions/FindPreAndSuccessor.js');
var LowestCommonAncestorInBST  = require('./questions/LowestCommonAncestorInBST.js');







/**
  CREATE BST
*/
var binarySearchTree = new BinarySearchTree();
var arr = [170, 45, 75, 90, 802, 24, 2, 66];

for( var i =0; i<arr.length;i++){
	binarySearchTree.insert(arr[i]);
}



/**
 CREATE BST ENDS
*/



// console.log(binarySearchTree.contains(45));
// console.log(binarySearchTree.contains(46));

// printTree.call(binarySearchTree);

// console.log(TreeTraversal.preOrderTraversal.call(binarySearchTree));
// console.log(TreeTraversal.preOrderTraversalIterative.call(binarySearchTree));
// console.log(TreeTraversal.spiralTraversal.call(binarySearchTree));

// console.log(AddGreaterValuesToBST.call(binarySearchTree));

// printTree.call(binarySearchTree);


// Questions
let bst =  new BinarySearchTree();
bst._root =  constructBinarySearchTree([1, 7, 5, 50, 40, 10]);
printTree.call(bst);
// console.log(findPreAndSuccessor.call(bst, 10));

console.log(LowestCommonAncestorInBST.call(bst, 50,7))
