
function LinkedList(){
  this._head = null;
  this._length = 0;
}

function Node(val, nextNode = null){
    return {
      val,
      nextNode
    }
};

LinkedList.prototype = {
  constructor : LinkedList,

  add : function( val ){
      let node = Node(val);

      if( this._head == null ){
        this._head = node;
      }else{
        let currNode  = this._head;
        while( currNode.nextNode != null){
          currNode = currNode.nextNode;
        }
        currNode.nextNode = node;
      }
      this._length++;
  },

  getSize : function(){
    return this._length;
  },

  removeNodeByIndex : function( index ){
    if( index < 0 || index > this.getSize()-1){
      throw new RangeError('index provided is not between 0 and number of elements')
    }

    if( index == 0 ){
      this._head = this._head.nextNode;
      return ;
    }
    // We need to get the previous pointer to index
    // remove pointer to

    let pointerIdx =  0;
    let currNode = this._head;
    while( pointerIdx < index-1 ){
        currNode =  currNode.nextNode;
    }
    let nodeToRemove = currNode.nextNode;
    let nodeToPatch =  null;
    if( nodeToRemove ){
        nodeToPatch = nodeToRemove.nextNode;
    }
    currNode.nextNode = nodeToPatch;
  },

  getList : function(){
    let arr =  [];
    let currNode  = this._head;

    while( currNode ){
      arr.push( currNode.val);
      currNode = currNode.nextNode;
    }
    return arr;
  },

  getHead : function(){
    return this._head;
  }

}
