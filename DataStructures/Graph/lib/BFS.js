

module.exports =  function BFS( graphInst , initialVertex = 0){
    let visited  =  new Array(graphInst.adjacencyList.length).fill(false); // create a array storing the visted flag
    let queue =  [];
    queue.push(initialVertex);
    visited[initialVertex] = true;

    while( queue.length > 0){
        let currVertex =  queue.pop();
        console.log(currVertex);
        console.log(graphInst.getAdjacencyListByVertex(currVertex).getList());
        let adjacencyList =  graphInst.getAdjacencyListByVertex(currVertex);
        for( let node  of adjacencyList.getList()){

            if( !visited[node.vertex] ){
               queue.push( node.vertex);
               visited[ node.vertex ] = true;
            }
        }
    }
    return queue;
}
