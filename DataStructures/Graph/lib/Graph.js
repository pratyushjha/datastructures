/**
 * Grpah is the data structure which consists of
 * two components
 * 1. A Finite set of vertices called nodes.
 * 2. A finite set of ordered pair of the form
 * ( u , v ) called the edge.
 * Two ways to represent graph:
 *  1. Adjacency matrix
 *  2. Adjacency List
 *  There are other representations like
 *  1. Incidence matrix
 *  2. Incidence List
 *
 *  Adjacency Matrix is a VXV matrix .
 *    Kind of mapping of vertices between i and j where i is row index
 *    and j is column index. If the value is 0 then it means we do
 *    not have a edge from i to j. We can also represent the weight
 *    of the edge through the matrix
 *
 *  Pros : Easier to implement , Removing, Searching can be done in O(1) time.
 *  Cons : Consumes more space O(V^2)
 *
 *
 *  Adjacency List :
 *    Represented via an array of LLs. Size of array is equal to number of vertices
 *    Linked List on each index represent the list of adjacent nodes. We can store
 *    weight of edges in the nodes itself.
 *
*/

let LinkedList  =  require('../../LinkedList/lib/LinkedList.js');

function Graph(countOfVertices = 0, isDirected = true){
    this.adjacencyList =  new Array(countOfVertices).fill(0).map(()=> new LinkedList());
    this._isDirected  = isDirected;
}

Graph.prototype.addEdge =  function(startVertex, endVertex , weight = 1){
  let startLL  = this.adjacencyList[startVertex];
  let endLL =  this.adjacencyList[endVertex];

  if( startLL && endLL ){
    startLL.addNode({ vertex: endVertex, weight});
    if(!this._isDirected){
     endLL.addNode({ vertex : startVertex, weight});
    }
  }
}

Graph.prototype.printGraph =  function(){
  for( let key =  0 , len = this.adjacencyList.length; key < len ; key++ ){
      console.log( `For vertex ${key} adjacencyList is ${JSON.stringify(this.adjacencyList[key].getList())}`);
  }
}

Graph.prototype.getAdjacencyListByVertex = function( index ){
  return  this.adjacencyList[index];
}

module.exports =  Graph;
