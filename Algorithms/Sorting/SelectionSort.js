/**

Selection sort assumes two subarrays from the given array

1. is already sorted array 
2. the ohter is un sorted array

We pick the minimum value from the unsorted array and swap it here 

*/

function SelectionSort(arrayToSort){
	
	for(var i =0; i<arrayToSort.length;i++){

		var min  = i;
		for(var j=i+1;j<arrayToSort.length;j++){
			if(arrayToSort[j]<arrayToSort[min]){
				min = j;
			}
		}
		if(i!=min){
			swap(arrayToSort,i,min);
		}
	}
}
function swap(arr,firstIdx,lastIdx){
	var temp = arr[firstIdx];
	arr[firstIdx] = arr[lastIdx];
	arr[lastIdx]= temp;
}
module.exports = SelectionSort;
