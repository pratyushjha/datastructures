"use strict";
function quickSort(arrToSort){

	function swap(arr,firstIdx,secondIdx){
		var temp = arr[firstIdx];
		arr[firstIdx] = arr[secondIdx];
		arr[secondIdx] = temp;
	}
	function partition(arr,leftIndex,rightIndex){
		var pivot  = Math.floor((leftIndex+rightIndex)/2);
		var pivotVal = arr[pivot],
		i = leftIndex,
		j=rightIndex;

		while(i<=j){

			while(arr[i]<pivotVal){
				i++;
			}
			while(arr[j]>pivotVal){
				j--;
			}
			if(i<=j){
				swap(arr,i,j);
				i++;
				j--;
			}
		}
		return i;
	}

	function quickSortRecursion(arr,leftIndex,rightIndex){

		leftIndex = (typeof leftIndex != "number")?0:leftIndex;
		rightIndex= (typeof rightIndex != "number")?arr.length-1:rightIndex;
		var index = partition(arr,leftIndex,rightIndex);

		if(leftIndex<index-1){
			quickSortRecursion(arr,leftIndex,index-1);
		}
		if(rightIndex>index){
			quickSortRecursion(arr,index,rightIndex);
		}
	}
	quickSortRecursion(arrToSort);
}

module.exports = quickSort;
