function InsertionSort(arrOfValues){
	if(!Array.isArray(arrOfValues)){
		return null;
	}

	if(arrOfValues.length<2){
		return arrOfValues;
	}
	var key;
	for(var i =1; i<arrOfValues.length;i++){
		key = arrOfValues[i];
		for(var j= i-1; j>=0;j--){

			if(key<arrOfValues[j]){
				arrOfValues[j+1] = arrOfValues[j];
				arrOfValues[j]= key;
			}else{
				break;
			}
		}
	}
	return arrOfValues;
}

module.exports = InsertionSort;