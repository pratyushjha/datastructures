function CountingSort(arrayToSort, min, max) {
    let countArr = [], returnArr = [];

    for (let i = min; i <= max; i++) {
        countArr[i] = 0;
    }
    for (let i = 0; i < arrayToSort.length; i++) {
        countArr[arrayToSort[i]] += 1;
    }
    for (let i = 0; i < countArr.length; i++) {
        let count = countArr[i];
        while (count > 0) {
            returnArr.push(i);
            count--;
        }
    }
    return returnArr;
}
module.exports = CountingSort;