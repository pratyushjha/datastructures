function RadixSort(arrayToSort, min, max) {

    return CountingSort(arrayToSort, min, max, 1);
}

function findNthDigit(num, n) {
    return (num % Math.pow(10, n) - num % Math.pow(10, n - 1)) / (Math.pow(10, n - 1));
}
function CountingSort(arrayToSort, min, max, exp) {
    let countArr = [], returnArr = [];
    for (let i = min; i <= max; i++) {
        countArr[i] = 0;
    }
    for (let i = 0; i < arrayToSort.length; i++) {
        countArr[findNthDigit(arrayToSort[i], exp)]++;
    }
    for (let i = 1; i < countArr.length; i++) {
        countArr[i] += countArr[i - 1];
    }

    for (let i = arrayToSort.length - 1; i >= 0; i--) {
        returnArr[countArr[findNthDigit(arrayToSort[i], exp)] - 1] = arrayToSort[i];
        countArr[findNthDigit(arrayToSort[i], exp)]--;
    }
    arrayToSort = returnArr;
    if (exp < 3) {
        return CountingSort(arrayToSort, min, max, exp + 1)
    }
    else {
        return arrayToSort;
    }

}
module.exports = RadixSort;