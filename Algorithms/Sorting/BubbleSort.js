/**
 Bubble Sort works in a way that
 1. You basically start from the rightMost element (index i) of the array
 2. for i-1 till 0 , find if any element is greater than element , then swap
 Thus right most element is bubbled up to left side

 */

function BubbleSort(arrayToSort) {

    for (var i = arrayToSort.length - 1; i >= 0; i--) {

        for (var j = i - 1; j >= 0; j--) {
            if (arrayToSort[i] < arrayToSort[j]) {
                swap(arrayToSort, i, j);
            }
        }
    }
}
function swap(arr, firstIdx, lastIdx) {
    var temp = arr[firstIdx];
    arr[firstIdx] = arr[lastIdx];
    arr[lastIdx] = temp;
}
module.exports = BubbleSort;