/**
 *
 * @param arrayToSort
 * @returns {*}
 * @constructor
 *
 * 1. We take a gap equal to length/2 diminished by 2 on every iteration
 * 2.
 *
 *
 * If size of array is 6
 * gap is 3
 *
 * i from 0 to less than 3(2)
 *  j = i;
 *   inside each loop do a while loop
 *    if(arr[j]>arr[j+gap])=> swap
 *    j =  j-gap
 *
 * gap -3 , loop 3
 *
 * 0 - 3
 * 1 - 4
 * 2 - 5
 *
 * gap -1 , loop 5
 *
 * 0 -  1
 * 1 -  2
 * 0 - 1
 *
 * 2 - 3
 * 3 - 4
 *
 *
 */


function ShellSort(arrayToSort) {
    let length = arrayToSort.length;

    for (let gap = Math.floor(length / 2); gap > 0; gap = Math.floor(gap / 2)) {

        for (let i = 0; i < length - gap; i++) {
            let j = i;
            while (true) {
                if (arrayToSort[j] > arrayToSort[j + gap]) {
                    swap(arrayToSort, j, j + gap);
                    j = j - gap;
                } else {
                    break;
                }
            }
        }
    }
    return arrayToSort;
}

function swap(arr, i, j) {

    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}


module.exports = ShellSort;