/**
 *
 * @param arrayToBeSorted
 * @returns {*}
 * @constructor
 *
 *
 */


function MergeSortRecursive(arrayToBeSorted){
	
	if(!Array.isArray(arrayToBeSorted)){
		return null;
	}
	if(arrayToBeSorted.length<2){
		return arrayToBeSorted;
	}

	function merge(leftArr,rightArr){
		var resultArr=[],
		leftIdx =0,rightIdx=0;

		while(leftIdx<leftArr.length&&rightIdx<rightArr.length){

			if(leftArr[leftIdx]<rightArr[rightIdx]){
				resultArr.push(leftArr[leftIdx]);
				leftIdx++;
			}else{
				resultArr.push(rightArr[rightIdx]);
				rightIdx++;
			}
		}
		resultArr= resultArr.concat(leftArr.slice(leftIdx)).concat(rightArr.slice(rightIdx));
		return resultArr;
	}

	function mergeSortRecursionFunction(arr){
		if(arr.length<2){
			return arr;
		}
		
		var midIdx = Math.floor((arr.length)/2);
		var leftArr =arr.slice(0,midIdx);// mergeSortRecursionFunction(arr,startIdx,midIdx);
		var rightArr = arr.slice(midIdx,arr.length);// mergeSortRecursionFunction(arr,midIdx+1,EndIdx);
		return merge(mergeSortRecursionFunction(leftArr),mergeSortRecursionFunction(rightArr));
	}
	return mergeSortRecursionFunction(arrayToBeSorted,0,arrayToBeSorted.length-1);
}
module.exports = MergeSortRecursive;